package com.arielsartistry.viewdrop;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.Log;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import programs.ColorShaderProgram;

import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glClearColor;
import static android.opengl.Matrix.orthoM;

/**
 * Created by Ariel on 2/12/14.
 */
public class ChildRenderer implements GLSurfaceView.Renderer{

    private Context context;

    private final float[] projectionMatrix = new float[16];
    private ColorShaderProgram colorProgram;

    private boolean showingTouch;
    private float x, y;

    public ChildRenderer(Context context)
    {
        this.context = context;
    }

    @Override
    public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {
        //ORANGE
        glClearColor(1f, 1f, 1f, 1f);
        colorProgram = new ColorShaderProgram(context);
    }

    @Override
    public void onSurfaceChanged(GL10 glUnused, int width, int height)
    {
        Log.d("Main", "1height: " + height + " w: " + width);

        final float aspectRatio = width > height ? (float) width / (float) height : (float) height / (float) width;

        if (width > height) {
            // Landscape
            orthoM(projectionMatrix, 0, -aspectRatio, aspectRatio, -1f, 1f, -1f, 1f);
        } else {
            // Portrait or square
            orthoM(projectionMatrix, 0, -1f, 1f, -aspectRatio, aspectRatio, -1f, 1f);
        }
    }

    @Override
    public void onDrawFrame(GL10 glUnused) {
        glClear(GL_COLOR_BUFFER_BIT);

        colorProgram.useProgram();
        colorProgram.setUniforms(projectionMatrix);

        if(showingTouch)
        {
            Touch t = new Touch();
            t.setCords(x*1.34f , y-1f);
            t.bindData(colorProgram);
            t.draw();
        }
    }

    public void showTouch(float x, float y)
    {
        showingTouch = true;
        this.x = x;
        this.y = y;
    }

    public void hideTouch()
    {
        showingTouch = false;
    }
}

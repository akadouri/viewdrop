package com.arielsartistry.viewdrop;

import android.app.Activity;
import android.graphics.Color;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;

public class MainActivity extends Activity {

    private final String TAG = "Main";

    private GLSurfaceView mommyView;
    private ImageView trash;

    private ArrayList<LinearLayout> view;
    private RelativeLayout layout;
    private boolean rendering;

    int curView = 0;
    boolean viewAdded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layout = new RelativeLayout(this);
        layout.setBackgroundColor(Color.parseColor("#FFFFFF"));
        // layout.setOrientation(LinearLayout.VERTICAL);

        final MommyRenderer mommyRenderer = new MommyRenderer(this);

        mommyView = new GLSurfaceView(this);
        mommyView.setEGLContextClientVersion(2);
        mommyView.setRenderer(mommyRenderer);
        rendering = true;
        mommyView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 100));
        layout.addView(mommyView);

        trash = new ImageView(this);
        trash.setImageResource(R.drawable.bomb);
        RelativeLayout.LayoutParams trashParams = new RelativeLayout.LayoutParams(100, 100);
        trashParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, trash.getId());
        trashParams.addRule(RelativeLayout.ALIGN_RIGHT);
        layout.addView(trash, trashParams);
        trash.setVisibility(View.GONE);

        view = new ArrayList<LinearLayout>();

        mommyView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event != null) {
                    //Convert to normalized device coordinates
                    final float normalizedX = (event.getX(event.getPointerCount() - 1) / (float) v.getWidth()) * 2 - 1;
                    final float normalizedY = -((event.getY(event.getPointerCount() - 1) / (float) v.getHeight()) * 2 - 1);


                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        mommyView.queueEvent(new Runnable() {
                            @Override
                            public void run() {
                                mommyRenderer.showTouch(normalizedX, normalizedY);
                            }
                        });
                    } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
                        if(normalizedY < -1f)
                        {
                            // Log.d(TAG, "show child");
                            if(!viewAdded)
                            {
                                view.add((LinearLayout) MainActivity.this.getLayoutInflater().inflate(R.layout.plate, null));
                            }

                            mommyView.queueEvent(new Runnable() {
                                @Override
                                public void run() {
                                    mommyRenderer.hideTouch();
                                }
                            });

                            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                            params.leftMargin = (int)event.getX() - view.get(0).getWidth() / 2;
                            params.topMargin = (int)event.getY();
                            view.get(curView).setLayoutParams(params);

                            if(!viewAdded)
                            {
                                layout.addView(view.get(curView));
                                viewAdded = true;
                            }
                        }else
                        {
                            mommyView.queueEvent(new Runnable() {
                                @Override
                                public void run() {
                                    mommyRenderer.showTouch(normalizedX, normalizedY);
                                }
                            });
                        }
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        //Log.d(TAG, "normx: " + normalizedX + " normy: " + normalizedY);
                        //Log.d(TAG, "x: " + event.getX() + " y: " + event.getY());

                        if(view.size() >= curView && viewAdded)
                        {
                            trash.setVisibility(View.VISIBLE);

                            view.get(curView).setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    if (event != null) {
                                        Log.d(TAG, "tX: " + event.getRawX() + " tY: " + event.getRawY());

                                        //Convert to normalized device coordinates
                                        //final float normalizedX = (event.getX(event.getPointerCount() - 1) / (float) v.getWidth()) * 2 - 1;
                                        //final float normalizedY = -((event.getY(event.getPointerCount() - 1) / (float) v.getHeight()) * 2 - 1);

                                        if (event.getAction() == MotionEvent.ACTION_MOVE) {

                                            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

                                            int x = (int)event.getRawX() - view.get(0).getWidth() / 2;
                                            int y = (int)event.getRawY();
                                            params.leftMargin = x;
                                            params.topMargin = y;
                                            v.setLayoutParams(params);

                                            if(x < 100 && y > 1400)
                                            {
                                                layout.removeView(v);
                                                view.remove(v);
                                                curView--;
                                            }
                                        }

                                        return true;
                                    }else
                                    {
                                        return false;
                                    }
                                }
                            });
                        }else
                        {
                            trash.setVisibility(View.GONE);
                        }

                        if(viewAdded)
                        {
                            curView++;
                            viewAdded = false;
                        }


                        if (normalizedY < -1f) {

                            //If up is outside the bounds of the view then draw the item
                            //TextView tv = new TextView(MainActivity.this);
                            //tv.setText("new view!");
                            // layout.addView(tv);
                        }
                    }
                    return true;
                } else {
                    return false;
                }
            }
        });
        setContentView(layout);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (rendering) {
            mommyView.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (rendering) {
            mommyView.onResume();
        }
    }
}
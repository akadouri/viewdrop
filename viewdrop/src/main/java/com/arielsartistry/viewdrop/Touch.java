package com.arielsartistry.viewdrop;

import data.VertexArray;
import programs.ColorShaderProgram;

import static android.opengl.GLES20.GL_POINTS;
import static android.opengl.GLES20.glDrawArrays;

/**
 * Created by Ariel on 2/12/14.
 */
public class Touch{

    private static final int POSITION_COMPONENT_COUNT = 2;
    private static final int COLOR_COMPONENT_COUNT = 3;
    private static final int STRIDE =
            (POSITION_COMPONENT_COUNT + COLOR_COMPONENT_COUNT)
                    * 4;
    private static final float[] VERTEX_DATA = {
            // Order of coordinates: X, Y, R, G, B
            0f, 0f, 0f, 0f, 1f,};
    private VertexArray vertexArray;

    public Touch() {
        vertexArray = new VertexArray(VERTEX_DATA);
    }

    public void bindData(ColorShaderProgram colorProgram) {
        vertexArray.setVertexAttribPointer(
                0,
                colorProgram.getPositionAttributeLocation(),
                POSITION_COMPONENT_COUNT,
                STRIDE);
        vertexArray.setVertexAttribPointer(
                POSITION_COMPONENT_COUNT,
                colorProgram.getColorAttributeLocation(),
                COLOR_COMPONENT_COUNT,
                STRIDE);
    }

    public void setCords(float x, float y)
    {
        float[] VERTEX_DATA = {
                // Order of coordinates: X, Y, R, G, B
                x, y, 0f, 0f, 1f,};

        vertexArray = new VertexArray(VERTEX_DATA);
    }

    public void draw() {
        glDrawArrays(GL_POINTS, 0, 1);
    }
}
